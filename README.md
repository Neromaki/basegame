![basegame-logo-small.png](https://bitbucket.org/repo/bXeKGk/images/1824670350-basegame-logo-small.png)

### What is BaseGame? ###

Base framework which can be forked as a base for building GameMaker games quicker, by adding the basic fundamental components in one place, which can be updated and forked for different projects, whilst always retaining the same base code.

BaseGame can be forked into either specific games, or into a secondary “base” for specific genres of games (e.g. a base “infinite scrolling shooter / platform” game, which can then be forked from)

Will contain items such as:

* Splash screen _(on top of default “made with gamemaker” screen, for company logo)_
* Title screen with menu
* Sub menus for common configuration options _(display, audio, controls etc)_
* Game room _(for adding the actual game into)_
* Game over / finish screen _(with restart, main menu options)_
* In-game menu _(options, quit etc)_
* Core control input support _(WASD, arrows and controllers)_
* Menu and/or in-game looping track support
* Base objects for players, enemies, props
* Base score keeping and game over conditions