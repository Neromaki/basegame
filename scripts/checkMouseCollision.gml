var target = self;
if( argument_count > 0) target = argument0;

/// Check mouse collision with self
return position_meeting( mouse_x, mouse_y, target );
