var label_text      = "Label";
var label_x         = self.x + self.sprite_width/2;
var label_y         = self.y + self.sprite_height/2;
var label_font      = fnt_menu_label;
var label_colour    = c_white;

if ( argument_count > 0 ) {
    label_text = argument[0];
}
if ( argument_count > 1 ) {
    label_x = argument[1];
}
if ( argument_count > 2 ) {
    label_y = argument[2];
}
if ( argument_count > 3 ) {
    label_font = argument[3];
}
if ( argument_count > 4 ) {
    label_colour = argument[4];
}

draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_set_color( label_colour );
draw_set_font( label_font );

draw_text( label_x, label_y, label_text);
