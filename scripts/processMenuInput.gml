switch( menu[| menu_position].type )
{
    case ( 'slider' ):
        // Which direction through the menu we are moving, default 0 (not moving anywhere)
        var slider_change = 0;
        // The value of the gamepads left stick vertical axis
        var axis_val = gamepad_axis_value( 0, gp_axislh );
        
        // Check input devices for anything relating to moving left the menu
        slider_change -= max( keyboard_check_pressed( vk_left ),
                            keyboard_check_pressed( ord("A") ),
                            ( gamepad_is_connected(0)
                                && ( gamepad_button_check_pressed( 0, gp_padl ) ||
                                    ( axis_val < -0.5  && !axis_freeze ) )
                            ), 0 );
                            
        // Check input devices for anything relating to moving right the menu
        slider_change += max( keyboard_check_pressed( vk_right ),
                            keyboard_check_pressed( ord("D") ),
                            ( gamepad_is_connected(0)
                                && ( gamepad_button_check_pressed( 0, gp_padr ) ||
                                    ( axis_val > 0.5  && !axis_freeze ) )
                            ), 0 );
                            
                            
        if ( slider_change != 0 ) {
            var ten_percent = lerp(menu[| menu_position].leftLimit, menu[| menu_position].rightLimit, 0.1) - menu[| menu_position].leftLimit;
            var xx =  menu[| menu_position].knob.x + ( ten_percent * slider_change );
            var menu_change = xx;
            axis_freeze = true;
            // Will unfreeze the axis after this duration (in seconds)
            alarm[0] = 0.25 * room_speed;
           
            // show_debug_message( "Knob X: " + string( menu[| menu_position].knob.x ) );
            // show_debug_message( "XX: " + string( xx ) );
            // show_debug_message( "Left limit: " + string( menu[| menu_position].leftLimit ) ); 
            // show_debug_message( "Right limit: " + string( menu[| menu_position].rightLimit ) );
           
            if (( menu_change > menu[| menu_position].leftLimit ) 
                && ( menu_change < menu[| menu_position].rightLimit ) )
            {
                show_debug_message( "Slider within limits" );
                 menu[| menu_position].knob.x = menu_change;
            }
            else if ( menu_change < menu[| menu_position].leftLimit)
            {
                show_debug_message( "Slider at left limit" );
                menu[| menu_position].knob.x = menu[| menu_position].leftLimit;
            }
            else if ( menu_change > menu[| menu_position].rightLimit)
            {
                show_debug_message( "Slider at right limit" );
                menu[| menu_position].knob.x = menu[| menu_position].rightLimit;
            }
            
            menu[| menu_position].knob.percentage = round(((menu[| menu_position].knob.x - menu[| menu_position].leftLimit) / (menu[| menu_position].rightLimit - menu[| menu_position].leftLimit)) * 100);
            show_debug_message( "Percentage: " + string( menu[| menu_position].knob.percentage ) );
        }
        break;
        
    case ( 'button' ):
    case ( 'switch' ):
    default:
        if ( gamepad_button_check_pressed( 0, gp_face1 ) || keyboard_check_pressed( vk_enter ) ) {
            // Set the current menu item to being pressed
            menu[| menu_position].beingPressed = true;
            // Activate the timer that - after the duration - will reset this menu item to being *not* pressed
            alarm[1] = 0.1 * room_speed;
            
            menu[| menu_position].doAction = true;
        }
        break;
}

