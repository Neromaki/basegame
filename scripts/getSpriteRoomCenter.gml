var sprite = argument0;
var room_center = getRoomCenter();
var sprite_center;

sprite_center['x'] = ( room_center['x'] - ( sprite_get_width( sprite ) / 2 ) );
sprite_center['y'] = ( room_center['y'] - ( sprite_get_height( sprite ) / 2 ) );

return sprite_center;
