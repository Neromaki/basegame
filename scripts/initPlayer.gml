#define initPlayer
show_debug_message('SCRIPT: initPlayer');

/// Initialise player

// If we've reached the max number of players
if( global.player_count >= global.max_players ) {
    return false;
}

global.player_count++;
self.player_id = global.player_count;

setupDefaultControls();
    
// If the player's control map .ini doesn't exist, create it with default controls
if( !file_exists( string( global.controls_ini_prefix ) + string( self.player_id ) + ".ini" ) ) {
    show_debug_message('LOG: ' + string( global.controls_ini_prefix ) + string( self.player_id ) + ".ini" + " doesn't exist, setting up defaults..");
    createPlayerControlsIni();
}
initPlayerControls();

#define setupDefaultControls
show_debug_message('SCRIPT: setupDefaultControls');
        
self.default_controls = ds_map_create();

switch(self.player_id)
{
    // Player 1
    case(1):
        // Keyboard
        self.default_controls[? 'keyboard_up'] = 'W';
        self.default_controls[? 'keyboard_down'] = 'A';
        self.default_controls[? 'keyboard_left'] = 'S';
        self.default_controls[? 'keyboard_right'] = 'D';
        self.default_controls[? 'keyboard_action1'] = 'G';
        self.default_controls[? 'keyboard_action2'] = 'H';
        self.default_controls[? 'keyboard_action3'] = 'Y';
        self.default_controls[? 'keyboard_action4'] = 'J';
        
        // Mouse
        self.default_controls[? 'mouse_action1'] = 'mb_left';
        self.default_controls[? 'mouse_action2'] = 'mb_right';
        
        // Gamepad
        self.default_controls[? 'gamepad_index'] = '0';  
        
        break;
        
        
    // Player 2
    case(2):
        // Keyboard
        self.default_controls[? 'keyboard_up'] = 'vk_up';
        self.default_controls[? 'keyboard_down'] = 'vk_down';
        self.default_controls[? 'keyboard_left'] = 'vk_left';
        self.default_controls[? 'keyboard_right'] = 'vk_right';
        self.default_controls[? 'keyboard_action1'] = 'vk_numpad1';
        self.default_controls[? 'keyboard_action2'] = 'vk_numpad2';
        self.default_controls[? 'keyboard_action3'] = 'vk_numpad3';
        self.default_controls[? 'keyboard_action4'] = 'vk_numpad0';
        
        // Gamepad
        self.default_controls[? 'gamepad_index'] = '1';  
        
        
    // Player 3
    case(3):     
        // Has no mouse input
        // Has no keyboard input 
        
        // Gamepad
        self.default_controls[? 'gamepad_index'] = '2';    
        break;
        
        
    // Player 4
    case(4):      
        // Has no mouse input
        // Has no keyboard input  
        
        // Gamepad
        self.default_controls[? 'gamepad_index'] = '3';    
        break;
}

// Gamepad (added for all players)     
self.default_controls[? 'gamepad_action1'] = 'gp_face1';
self.default_controls[? 'gamepad_action2'] = 'gp_face3';
self.default_controls[? 'gamepad_action3'] = 'gp_face4';
self.default_controls[? 'gamepad_action4'] = 'gp_face2';

#define createPlayerControlsIni
show_debug_message('SCRIPT: createPlayerControlsIni');

ini_open( string( global.controls_ini_prefix ) + string( self.player_id ) + ".ini" );

// Keyboard
ini_write_string("Keyboard", "up", self.default_controls[? 'keyboard_up']);
ini_write_string("Keyboard", "down", self.default_controls[? 'keyboard_down']);
ini_write_string("Keyboard", "left", self.default_controls[? 'keyboard_left']);
ini_write_string("Keyboard", "right", self.default_controls[? 'keyboard_right']);
ini_write_string("Keyboard", "action1", self.default_controls[? 'keyboard_action1']);
ini_write_string("Keyboard", "action2", self.default_controls[? 'keyboard_action2']);
ini_write_string("Keyboard", "action3", self.default_controls[? 'keyboard_action3']);
ini_write_string("Keyboard", "action4", self.default_controls[? 'keyboard_action4']);

// Mouse
if( ds_map_exists( self.default_controls, 'mouse_action1' ) ) { 
    ini_write_string("Mouse", "action1", self.default_controls[? 'mouse_action1']);
}
if( ds_map_exists( self.default_controls, 'mouse_action2' ) ) { 
    ini_write_string("Mouse", "action2", self.default_controls[? 'mouse_action2']); 
}
        

// Gamepad
ini_write_string("Gamepad", "index", self.default_controls[? 'gamepad_index']);  // Controller index number  
        
ini_write_string("Gamepad", "action1", self.default_controls[? 'gamepad_action1']);   // A / Green
ini_write_string("Gamepad", "action2", self.default_controls[? 'gamepad_action2']);   // X / Blue
ini_write_string("Gamepad", "action3", self.default_controls[? 'gamepad_action3']);   // Y / Yellow
ini_write_string("Gamepad", "action4", self.default_controls[? 'gamepad_action4']);   // B / Red

ini_close();

#define initPlayerControls
show_debug_message('SCRIPT: initPlayerControls');

// Which controller this player will be using (0 = first controller)
ini_open( string( global.controls_ini_prefix ) + string( self.player_id ) + ".ini" );
    self.gamepad_index = ini_read_string( 'Gamepad', 'index', self.default_controls[? 'gamepad_index']);
ini_close();

#define processPlayerInput
show_debug_message('SCRIPT: processPlayerInput');

// Process player's input

/*
*   Base control mapping
*
*   Defines the basic controls which each player will inherit
*   Key binding will be set per player
*/

/* 
*   KEYBOARD INPUT
*/

// Left
if(keyboard_check(self.left)) {
    // Do left action
} else {
    // Do left termination action
}

// Right
if(keyboard_check(self.right)) {
    // Do right action
} else {
    // Do right termination action
}

// Up
if(keyboard_check(self.up)) {
    // Do up action
} else {
    // Do up termination action
} 

// Down
if(keyboard_check(self.down)) {
    // Do down action
} else {
    // Do down termination action
} 

/*
*   MOUSE INPUT
*/




/*
*   CONTROLLER INPUT
*/
// Check there is a controller in the player's defined port
if(gamepad_is_connected(self.gp_index)) {
    // If so, set up the stick's deadzone to prevent gentle creeping
    gamepad_set_axis_deadzone(self.gp_index, 0.2);
    
    // If the controller's analog stick is being used in a horizontal fashion
    if(gamepad_axis_value(self.gp_index, gp_axislh) != 0) {
        // Set the player's horizontal speed equal to the stick value (-1 - 1, * the max_speed for a max of -5 or 5!)
        self.hspeed = (gamepad_axis_value(self.gp_index, gp_axislh) * self.max_speed);
    }
    // If the controller's analog stick is being used in a horizontal fashion
    if(gamepad_axis_value(self.gp_index, gp_axislv) != 0) {    
        // Set the player's horizontal speed equal to the stick value (-1 - 1, * the max_speed for a max of -5 or 5!)
        self.vspeed = (gamepad_axis_value(self.gp_index, gp_axislv) * self.max_speed);
    }
}