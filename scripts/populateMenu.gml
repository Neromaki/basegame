#define populateMenu
/// Populate menu
show_debug_message('SCRIPT: populateMenu');
var i;
prev_item = '';

for( i = 0; i < ds_list_size( menu_items ); i++; ) {
    var inst = instance_create( room_width / 2 - ( sprite_get_width( menu_items[| i] ) / 2 ), pos_y - ( sprite_get_height( menu_items[| i] ) / 2 ), menu_items[| i] );
    if ( inst.type == 'button' ) {
        inst.x = room_width / 2 - ( inst.sprite_width / 2);
        inst.y = pos_y - ( inst.sprite_height / 2 );
    }
        
    if( prev_item != '' ) {
        if( ( prev_item == 'slider' || prev_item == 'switch' ) && inst.type == 'button' ) {
            var adjustment = 30;
            inst.y = inst.y + adjustment;
            pos_y = pos_y + adjustment;
        }
    }
    ds_list_add( menu, inst );
    
    if ( menu_items[| i].type == 'slider' ) {
        var sprite_padding = menu_items[| i].sprite_height * 2;
    } else {
        var sprite_padding = menu_items[| i].sprite_height;
    }

    pos_y = ( pos_y + sprite_padding + menu_item_spacing );
    prev_item = inst.type;
}

#define populateControlsMenu
/// Populate menu
show_debug_message('SCRIPT: populateControlsMenu');
var i;
prev_item = '';

