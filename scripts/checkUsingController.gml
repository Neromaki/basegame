if( ( mouse_check_button_pressed( mb_any ) 
    || mouse_x != mouse_prev_x 
    || mouse_y != mouse_prev_y
    || keyboard_lastkey != -1 ) 
    && global.using_controller ) {
    // Some form of keyboard / mouse input detected, switch to KBM
    global.using_controller = false;    
    global.was_controller = true; 
}



if( !global.was_controller && global.using_controller ) {
    global.mouse_prev_x = mouse_x;
    global.mouse_prev_y = mouse_y;
}

