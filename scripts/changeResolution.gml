#define changeResolution
show_debug_message( "SCRIPT: changeResolution" );

var width = round (real( argument0 ) );
var height = round (real( argument1 ) );

show_debug_message( "DISPLAY: Setting to " + string(width) + " x " + string(height) );

//resizeRooms();

view_wview = width;
view_hview = height;

view_wport = width;
view_hport = height;

surface_resize( application_surface, view_wview, view_hview );
display_set_gui_size( view_wview, view_hview );
window_set_size( view_wview, view_hview );

#define resizeRooms
show_debug_message( "SCRIPT: resizeRooms" );

var i;
i = true;
rm = room_first;
var current_room = room;

while (i = true)
{
    show_debug_message( "resizing " + string(room_get_name(rm)));
    // Set the correct view in the room
    room_set_view(rm, 0, true,(room_width - view_wview) / 2, (room_height - view_hview) / 2, view_wview, view_hview, 0, 0, view_wview, view_hview, 0, 0, -1, -1, -1);
    
    // Continue to next room
    if (rm != room_last)
    {
        rm_prev = rm;
        rm = room_next(rm_prev);
    }
    
    // Stop if this is the last room
    else
    {
        i = false;
        if ( global.firstRun ) {
            room_goto( rm_splash );
        } else {
            room_goto(current_room);
        }
        instance_destroy();
    }
}