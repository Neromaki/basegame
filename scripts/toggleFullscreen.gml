show_debug_message( "SCRIPT: toggleFullscreen" );

//window_set_fullscreen( !window_get_fullscreen() );
if( window_get_fullscreen() ) {
    // Going windowed
    show_debug_message( "DISPLAY: GOING WINDOWED" );
    window_set_fullscreen( false );
} else {
    // Going fullscreen
    show_debug_message( "DISPLAY: GOING FULLSCREEN" );
    window_set_fullscreen( true );
}
