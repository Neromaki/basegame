/// Check mouse over object
/**
*   checkMouseOver
*
*   Checks if the mouse cursor is over the referencing object
*/
if(mouse_x >= self.x && mouse_x <= (self.x + self.sprite_width) &&
    mouse_y >= self.y && mouse_y <= (self.y + self.sprite_height)) {
    return true;
}
return false;
