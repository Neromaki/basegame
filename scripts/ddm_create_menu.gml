#define ddm_create_menu
/*
    CREDIT: Lycondone
    SOURCE: http://gmc.yoyogames.com/index.php?showtopic=363251
*/

/*
Creates a new drop down menu.

Argument0: X
Argument1: Y

Argument2: The default option on the menu

Argument3: Number of options to have before using a scrollbar.
(Must be larger than 2)

Argument4: Auto-sort (true or false)
Setting argument4 to true will auto-sort the list whenever a new option is added to the list.

Argument5: Auto-sort in ascending order (true or false)
If argument4 is true then set this to true or false to choose the order in which the list will be arranged.
(Ascending = true, Decending = false)

Argument6: Font to use on the menu

RETURNS: The ID to be used for other menu scripts.
*/

var a;
a = instance_create(argument0,argument1,pa_dropdown_menu);

a.autoSort = argument4;
a.autoSortAscending = argument5;

with (a) {list = ds_list_create();}
a.menuOption = argument2;
ddm_add_option(a,argument2);

if (argument3 < 3)
{a.maxScroll = 3;}
else
{a.maxScroll = argument3;}

a.font = argument6;

return a.id;

#define ddm_add_option
/*
Adds an option to the selected drop down menu.

Argument0: Drop down menu ID
Argument1: Option to add
*/

with (argument0)
{
    ds_list_add(list,argument1);
    totalSelections += 1;
    
    //Check if the menu is set to auto sort.
    if (autoSort = true)
    {ddm_sort(id,autoSortAscending);}
}

#define ddm_get_selection
/*
Returns the option currently selected in a drop down menu.

Argument0: Drop down menu ID
*/

return argument0.menuOption;

#define ddm_settings
/*
This script allows you to change some settings for a menu.

NOTE: All arguments can be set to -1 to not change the value.

Argument0: Drop down menu ID

Argument1: Menu Width (px)
Argument2: Menu Height (px)
Argument3: Menu Alpha

Argument4: Button Color
Argument5: Button Color on Mouse Hover
Argument6: Scroll Color
*/

with (argument0)
{
    if (argument1 != -1) {buttonWidth = argument1;}
    if (argument2 != -1) {buttonHeight = argument2;}
    if (argument3 != -1) {menuAlpha = argument3;}
    if (argument4 != -1) {buttonColor = argument4;}
    if (argument5 != -1) {buttonColorAlpha = argument5;}
    if (argument6 != -1) {scrollColor = argument6;}
}

#define ddm_sort
/*
Sorts out the list on a drop down menu.

Argument0: Drop down menu ID
Argument1: Set to true for an ascending list, false for decending.
*/

with (argument0) {ds_list_sort(list,argument1);}

#define mouse_in_area
/*
Returns true if the mouse is within an area.

Argument0: x1
Argument1: y1
Argument2: x2
Argument3: y2
*/

if (mouse_x >= argument0 and mouse_y >= argument1 and mouse_x <= argument2 and mouse_y <= argument3)
{return true;} else {return false;}