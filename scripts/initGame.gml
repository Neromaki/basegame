#define initGame
show_debug_message('SCRIPT: initGame');

// The max number of players this game can support
globalvar max_players;
global.max_players = 4;

// (DO NOT CHANGE!) The current player count, used to assign player IDs and controls
globalvar player_count;
global.player_count = 0;

globalvar controls_ini_prefix;
global.controls_ini_prefix = "controls_player";
globalvar settings_ini;
global.settings_ini = "settings.ini";


initHelperVariables();
initSettings();
initPlayer();

#define initHelperVariables
globalvar current_room;

globalvar was_controller;
global.was_controller = false;
globalvar using_controller;
global.using_controller = true;

globalvar mouse_prev_x;
global.mouse_prev_x = 0;
globalvar mouse_prev_y;
global.mouse_prev_y = 0;
#define initSettings
ini_open( global.settings_ini );


// Audio levels
global.sound_master  = round (real( ini_read_real("sound", "master", 50 ) ));
global.sound_ambient = round (real( ini_read_real("sound", "ambient", 50) ) );
global.sound_effects = round (real( ini_read_real("sound", "effects", 50) ) );
global.sound_music   = round (real( ini_read_real("sound", "music", 50) ) );


// Display
global.display_fullscreen = round ( ini_read_real("display", "fullscreen", 1) );

global.display_fullscreen_resolution_width = round ( ini_read_real("display", "fullscreen_width", display_get_width()) );
global.display_fullscreen_resolution_height = round ( ini_read_real("display", "fullscreen_height", display_get_height()) );

global.display_window_resolution_width = round ( ini_read_real("display", "windowed_width", 1280) );
global.display_window_resolution_height = round ( ini_read_real("display", "windowed_height", 720) ) ;

if ( global.display_fullscreen != window_get_fullscreen() ) {
    toggleFullscreen();
}
if ( window_get_fullscreen() ) {
    changeResolution( global.display_fullscreen_resolution_width,global.display_fullscreen_resolution_height );
} else {
    changeResolution( global.display_window_resolution_width,global.display_window_resolution_height );
}


ini_close();