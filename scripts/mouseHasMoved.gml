if( mouse_x != global.mouse_prev_x 
    || mouse_y != global.mouse_prev_y ) {
    show_debug_message('SCRIPT: mouseHasMoved');
    show_debug_message("mouse____y = " + string(mouse_y));
    show_debug_message("mouseprevy = " + string(global.mouse_prev_y));
    global.mouse_prev_x = mouse_x;
    global.mouse_prev_y = mouse_y;
    return true;
} else {
    return false;
}

